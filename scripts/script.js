// let firstNumber
// do {
//    firstNumber = +prompt('Enter first number');
// } while (isNaN(firstNumber));

let firstNumber = parseFloat(prompt('Enter first number'));
while (isNaN(firstNumber)) {
   firstNumber = parseFloat(prompt('The number is invalid. Please input numeric characters only.'));
}

let secondNumber = parseFloat(prompt('Enter second number'));
while (isNaN(secondNumber)) {
   secondNumber = parseFloat(prompt('The number is invalid. Please input numeric characters only.'));
}

let operator = prompt('Enter mathematical operator (+ - * /)');

function calcResult(firstNumber, secondNumber, operator) {
  switch (operator) {
    case '+':
      return firstNumber + secondNumber;
    case '-':
      return firstNumber - secondNumber;
    case '*':
      return firstNumber * secondNumber;
    case '/':
      if (secondNumber === 0) {
        return alert('Any number can never be divided by zero');
       }
        else {
        return firstNumber / secondNumber;
        }
  }
}

console.log(calcResult(firstNumber, secondNumber, operator));

